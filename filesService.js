const fs = require("fs");
const path = require('path');
const dataKeeperFolrder = './files/';
const allowedExt = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

const isExistFile = (res, err) => {
  if (err) {
    if (err.code == 'ENOENT') {
      res.status(400).send({ message: 'Failed. There is no file with such name', status: 400 });
    } else {
      res.status(500).send({ message: 'Failed. Server error', status: 500 });
    }
    return true;
  }
}

const isAllowedExtFile = (ext) => allowedExt.includes(ext);

function createFile(req, res, next) {
  const { filename, content } = req.body;
  if (!isAllowedExtFile(path.extname(dataKeeperFolrder + filename))) {
    res.status(400).send({ message: 'Failed. There is no allowed extansion file', status: 400 });
    return;
  }

  fs.writeFile(dataKeeperFolrder + filename, content, function (error) {
    if (error) throw error;
    res.status(200).send({ "message": "File created successfully" });
  });
}

function getFiles(req, res, next) {
  const allDataFiles = [];

  fs.readdirSync(dataKeeperFolrder).forEach(file => {
    allDataFiles.push(file);
  });

  res.status(200).send({
    "message": "Success",
    "files": allDataFiles,
  });
}

const getFile = (req, res, next) => {
  const { filename } = req.params;
  fs.readFile(dataKeeperFolrder + filename, { encoding: 'utf-8' }, function (err, data) {
    if (isExistFile(res, err)) return;
    const fileContent = fs.readFileSync(dataKeeperFolrder + filename, "utf8");
    res.status(200).send({
      "message": "Success",
      "filename": filename,
      "content": fileContent,
      "extension": path.extname(dataKeeperFolrder + filename).slice(1),
      "uploadedDate": fs.statSync(dataKeeperFolrder + filename).mtime
    });
  });
}
const putFile = (req, res, next) => {
  console.log("working");
  const { filename, content } = req.body;
  fs.writeFile(dataKeeperFolrder + filename, content, function (error) {
    console.log(content);
    if (isExistFile(res, err)) return;
    if (error) throw error;
    res.status(200).send({
      "message": "File update successfully",
      "filename": filename,
      "content": content,
      "extension": path.extname(dataKeeperFolrder + filename).slice(1),
      "uploadedDate": fs.statSync(dataKeeperFolrder + filename).mtime
    });
  });
}

const deleteFile = (req, res, next) => {
  console.log("working");
  const { filename } = req.params;
  console.log(dataKeeperFolrder + filename);
  fs.unlink(dataKeeperFolrder + filename, (err) => {
    if (isExistFile(res, err)) return;
    res.status(200).send({ "message": "File delete successfully" });
  });
}
// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  putFile,
  deleteFile
}
